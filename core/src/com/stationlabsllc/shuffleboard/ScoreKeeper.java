/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard;

public class ScoreKeeper {

	public enum player {
		RED_PLAYER, BLUE_PLAYER
	};
	
	private static int[] score;
	
	private player lastPlayerToScore = player.RED_PLAYER;

	public ScoreKeeper() {
		
		score = new int[player.values().length];
		for (int p = 0; p < player.values().length; p++){
			score[p] = 0;
		}
		
	}

	public int[] getScore() {
		return score;
	}
	
	public int getScore(ScoreKeeper.player player){
		return score[player.ordinal()];
	}

	public void addScore(ScoreKeeper.player player, int points){
		ScoreKeeper.score[player.ordinal()] += points;
		setLastPlayerToScore(player);
	}
	
	public void setScore(int[] score) {
		ScoreKeeper.score = score;
	}
	
	public void setScore(ScoreKeeper.player player, int score){
		ScoreKeeper.score[player.ordinal()] = score;
	}

	public player getLastPlayerToScore() {
		return lastPlayerToScore;
	}

	public void setLastPlayerToScore(player lastPlayerToScore) {
		this.lastPlayerToScore = lastPlayerToScore;
	}

}
