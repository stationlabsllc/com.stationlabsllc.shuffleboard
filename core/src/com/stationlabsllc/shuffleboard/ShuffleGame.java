/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.stationlabsllc.shuffleboard.ScoreKeeper.player;
import com.stationlabsllc.shuffleboard.gamescreens.GameEnd;
import com.stationlabsllc.shuffleboard.gamescreens.GameMenu;
import com.stationlabsllc.shuffleboard.gamescreens.MainPlay;
import com.stationlabsllc.shuffleboard.gamescreens.OverScreen;
import com.stationlabsllc.shuffleboard.gamescreens.ScoreBoard;

public class ShuffleGame extends Game {

	private ScoreKeeper score;

	public enum gameScreens {
		GAME_MENU, GAME_PLAY, SCORE_BOARD, GAME_END
	};

	public gameScreens selectedScreen = gameScreens.GAME_MENU;

	@Override
	public void create() {
		this.score = new ScoreKeeper();
		setScreen(new GameMenu());
	}

	@Override
	public void render() {
		OverScreen currentScreen = getScreen();

		// update the screen
		currentScreen.render(Gdx.graphics.getDeltaTime());

		if (currentScreen.isDone()) {
			currentScreen.dispose();

			if (selectedScreen.ordinal() == gameScreens.values().length - 1) {
				// set to the beginning screen
				this.score.setScore(player.BLUE_PLAYER, 0);
				this.score.setScore(player.RED_PLAYER, 0);
				selectedScreen = gameScreens.GAME_MENU;
			} else {
				boolean weHaveAWinner = score.getScore(player.RED_PLAYER) >= 15
						|| score.getScore(player.BLUE_PLAYER) >= 15;

				if (selectedScreen == gameScreens.SCORE_BOARD && !weHaveAWinner) {
					selectedScreen = gameScreens.GAME_PLAY;
				} else {
					// else increment the enum
					selectedScreen = gameScreens.values()[selectedScreen
							.ordinal() + 1];
				}
			}

			switch (selectedScreen.ordinal()) {
			case 0:
				setScreen(new GameMenu());
				break;

			case 1:
				setScreen(new MainPlay(score));
				break;

			case 2:
				setScreen(new ScoreBoard(score));
				break;

			case 3:
				setScreen(new GameEnd());
				break;

			default:
				break;
			}
		}
	}

	@Override
	public OverScreen getScreen() {
		return (OverScreen) super.getScreen();
	}
}
