/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard.gamepieces;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.stationlabsllc.shuffleboard.ScoreKeeper;
import com.stationlabsllc.shuffleboard.ScoreKeeper.player;

/**
 * Puck class for shuffle puck game
 * 
 * @author Jared Perez
 * 
 */
public class Puck {

	public enum rot {
		FLAT, RIGHT, LEFT, BACK
	}
	
	private boolean inPlay = false;

	private rot rotation = rot.FLAT;

	/**
	 * Owner of this puck (instance) or player
	 */
	private ScoreKeeper.player owner = player.RED_PLAYER;

	/**
	 * Identity of puck
	 */
	private short ident;

	/**
	 * Velocity vector of puck
	 */
	private Vector3 vel;

	/**
	 * Puck model instance
	 */
	private ModelInstance pModInst;

	/**
	 * Acceleration (deceleration)
	 */
	private static final float accel = -10f;

	/**
	 * Puck radius
	 */
	private static final float radius = 2.3125f;

	/**
	 * Declaration of puck object
	 * 
	 * @param model
	 *            Model to use for puck
	 * @param x
	 *            X Position
	 * @param y
	 *            Y Position
	 * @param z
	 *            Z Position
	 */
	public Puck(Model model, float x, float y, float z) {
		this.pModInst = new ModelInstance(model, x, y, z);

		this.vel = new Vector3();
	}

	/**
	 * General update of the puck
	 * 
	 * @param delta
	 *            time delta from last update
	 */
	public void update(float delta) {
		updatePos(delta);
		updateVel(delta);
		updateRotation(delta);
	}

	private void updateRotation(float delta) {
		Vector3 rotVect = new Vector3();
		switch (rotation) {
		case FLAT:
			return;
		case LEFT:
			rotVect = new Vector3(-1, 0, 0);
			break;
		case RIGHT:
			rotVect = new Vector3(1, 0, 0);
			break;
		case BACK:
			rotVect = new Vector3(0, 0, -1);
			break;
		default:
			break;
		}
		
		float degrees = 2f;
		
		pModInst.transform.rotate(rotVect, degrees);
	}

	/**
	 * Update the position based on the velocity of the puck
	 * 
	 * @param delta
	 */
	private void updatePos(float delta) {
		this.pModInst.transform
				.trn(delta * vel.x, delta * vel.y, delta * vel.z);
	}

	/**
	 * Update the velocity, slow down the puck
	 * 
	 * @param delta
	 *            Delta in time from last update
	 */
	private void updateVel(float delta) {
		if (vel.isZero())
			return;

		float dt = delta * accel;
		float len = vel.len();
		if (len + dt < 0) {
			vel = vel.scl(0);
		} else {
			dt = (vel.len() + dt) / vel.len();
			vel = vel.scl(dt);
		}

		return;
	}

	/**
	 * Print information about the puck
	 */
	public void printPuckInfo() {
		String infoStr = String.format("ID: %d V(%2.3f, %2.3f, %2.3f)", ident,
				vel.x, vel.y, vel.z);
		System.out.println(infoStr);
	}

	// Auto generated getters and setters

	public ScoreKeeper.player getOwner() {
		return owner;
	}

	public void setOwner(ScoreKeeper.player owner) {
		this.owner = owner;
	}

	public Vector3 getVelocity() {
		return vel;
	}

	public void setVelocity(Vector3 velocity) {
		this.vel = velocity;
	}

	public static float getRadius() {
		return radius;
	}

	public short getIdent() {
		return ident;
	}

	public void setIdent(short ident) {
		this.ident = ident;
	}

	public ModelInstance getpModInst() {
		return pModInst;
	}

	public void setpModInst(ModelInstance pModInst) {
		this.pModInst = pModInst;
	}

	public rot getRotation() {
		return rotation;
	}

	public void setRotation(rot rotation) {
		this.rotation = rotation;
	}

	public boolean isInPlay() {
		return inPlay;
	}

	public void setInPlay(boolean inPlay) {
		this.inPlay = inPlay;
	}
}
