/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard.gamepieces;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;

public class Deck {

	private ModelInstance pDeck;
	private float width;
	private float height;
	private float length;
	private float halfWidth;
	private float halfLength;
	private final float threePointWidth = 6f / 96f;
	private final float twoPointWidth = 6f / 96f;
	private final float onePointWidth = 6f / 96f;
	private float fourPointLine;
	private float threePointLine;
	private float twoPointLine;
	private float onePointLine;
	private float screenScaleFactor;
	
	public Deck(Model model, float x, float y, float z) {
		setpDeck(new ModelInstance(model, x, y, z));
	}

	public ModelInstance getpDeck() {
		return pDeck;
	}

	public void setpDeck(ModelInstance pDeck) {
		this.pDeck = pDeck;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
		this.halfWidth = width / 2;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
		this.halfLength = length / 2f;
		// if the puck is beyong length - puck raduis, part of
		// the puck is hanging off the endge of the table.
		this.fourPointLine = length - Puck.getRadius();
		this.threePointLine = length - length * threePointWidth;
		this.twoPointLine = threePointLine - length * twoPointWidth;
		this.onePointLine = twoPointLine - length * onePointWidth;
	}

	public float getHalfWidth() {
		return halfWidth;
	}

	public void setHalfWidth(float halfWidth) {
		setWidth(halfWidth * 2f);
	}

	public float getHalfLength() {
		return halfLength;
	}

	public void setHalfLength(float halfLength) {
		setLength(halfLength * 2f);
	}
	
	public float getFourPointLine(){
		return fourPointLine;
	}

	public float getThreePointLine() {
		return threePointLine;
	}

	public float getTwoPointLine() {
		return twoPointLine;
	}

	public float getOnePointLine() {
		return onePointLine;
	}

	public void setScaleFactorByScreenWidth(float screenWidth) {
		if (screenWidth == 0f) {
			return;
		}

		this.screenScaleFactor = this.width / screenWidth;
	}

	public float getScaledPosition(float screenPos) {
		return this.screenScaleFactor * screenPos;
	}
}
