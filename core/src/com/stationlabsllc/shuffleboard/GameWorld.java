/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.assets.loaders.ModelLoader.ModelParameters;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.model.data.ModelData;
import com.badlogic.gdx.graphics.g3d.utils.TextureProvider;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.JsonReader;
import com.stationlabsllc.shuffleboard.ScoreKeeper.player;
import com.stationlabsllc.shuffleboard.gamepieces.Deck;
import com.stationlabsllc.shuffleboard.gamepieces.Puck;
import com.stationlabsllc.shuffleboard.gamepieces.Puck.rot;

public class GameWorld {

	public static enum GameState {
		START, PLAY, PAUSE, ROUND_OVER
	};

	static enum PlayState {
		AIM, SHOOT, WATCH, RETURN, REST
	}

	private boolean pucksInMotion = false;
	private boolean puckIsLoaded = false;
	private GameState gameState = GameState.START;
	private Deck deck;
	private List<Puck> pucks = new ArrayList<Puck>();
	private Model deckModel;
	private int currentPuck;
	private boolean doneLoading = false;

	private final int pucks_per_round = 4;

	private ScoreKeeper score;

	private final float puck_start_forward = 0f;
	private final float puck_start_up = -8f;

	/**
	 * Game world constructor
	 * 
	 * @param screenWidth
	 *            Screen width in pixels
	 * @param screenHeight
	 *            Screen height in pixels
	 */
	public GameWorld(float screenWidth, float screenHeight, ScoreKeeper score) {
		// Making the deck
		// ModelBuilder modelBuilder = new ModelBuilder();
		float length = 200f;
		float height = 2;
		float width = 50f;

		this.currentPuck = 0;

		// Making the deck
		ModelLoader<ModelParameters> deckLoader = new G3dModelLoader(
				new JsonReader());

		deckModel = deckLoader.loadModel(Gdx.files.getFileHandle(
				"models/shuffle_deck.g3dj", FileType.Internal));

		/*
		 * deckModel = modelBuilder.createBox(length, height, width, new
		 * Material( ColorAttribute.createDiffuse(Color.ORANGE)), Usage.Position
		 * | Usage.Normal);
		 */
		setDeck(new Deck(deckModel, length / 2f, 0f, 0f));
		deck.setHeight(height);
		deck.setLength(length);
		deck.setWidth(width);
		deck.setScaleFactorByScreenWidth(screenWidth);
		deck.getpDeck().transform.scale(17.0f, 17.0f, 25.5f);
		deck.getpDeck().calculateTransforms();

		// Making the pucks
		ModelLoader<ModelParameters> puckLoader = new G3dModelLoader(
				new JsonReader());
		ModelData blue_data = puckLoader.loadModelData(Gdx.files
				.internal("models/shuffle_puck_blue.g3dj"));
		ModelData red_data = puckLoader.loadModelData(Gdx.files
				.internal("models/shuffle_puck_red.g3dj"));
		TextureProvider blue_tex = new TextureProvider.FileTextureProvider();
		blue_tex.load("models/textured_blue_puck.png");
		TextureProvider red_tex = new TextureProvider.FileTextureProvider();
		red_tex.load("models/textured_red_puck.png");
		Model blue_puck = new Model(blue_data, blue_tex);
		Model red_puck = new Model(red_data, red_tex);

		/*
		 * final Texture puckTexture = new Texture(
		 * Gdx.files.internal("models/puck.png"), Format.RGB888, true);
		 * puckTexture.setFilter(TextureFilter.MipMap, TextureFilter.Linear);
		 * puckModel.materials.get(0).set(
		 * TextureAttribute.createDiffuse(puckTexture));
		 */

		// Manual add pucks into game world to create different scenarios
		int cnt = 0;
		player lastPlayer = score.getLastPlayerToScore();

		for (int c = 0; c < pucks_per_round; c++) {
			for (ScoreKeeper.player p : ScoreKeeper.player.values()) {
				player thisPlayer = player.RED_PLAYER;

				if (lastPlayer == player.RED_PLAYER) {
					thisPlayer = p;
				} else {
					if (p == player.RED_PLAYER) {
						thisPlayer = player.BLUE_PLAYER;
					} else {
						thisPlayer = player.RED_PLAYER;
					}
				}

				if (thisPlayer == player.RED_PLAYER) {
					pucks.add(new Puck(red_puck, 0, 0, 0));
				} else {
					pucks.add(new Puck(blue_puck, 0, 0, 0));
				}

				pucks.get(cnt).setIdent((short) cnt);
				pucks.get(cnt).setOwner(thisPlayer);
				clearPuckToIdlePosition(cnt);
				cnt += 1;
			}
		}

		this.score = score;
		setDoneLoading(true);
	}

	/**
	 * Update the game world
	 * 
	 * @param delta
	 *            Delta in time from last update
	 */
	public void update(float delta) {

		if (gameState == GameState.ROUND_OVER) {
			checkNeedToClearPucks();
			return;
		}

		if (pucksInMotion) {
			updatePuckPos(delta);
			checkAllPucksCollide(0, pucks);
			checkOffTable();
			checkInMotion();
		} else {
			if (!puckIsLoaded) {
				checkNeedToClearPucks();
				loadNextPuck();
			}
		}
	}

	/**
	 * Check if the puck is under the center line
	 * 
	 * @param puckId
	 *            Puck identity
	 * @return true if under center line
	 */
	public boolean isUnderCenterLine(int puckId) {
		if (!getPucks().get(puckId).isInPlay())
			return false;

		Vector3 position = new Vector3();
		getPucks().get(puckId).getpModInst().transform.getTranslation(position);

		return position.x > 0 && position.x < getDeck().getHalfLength();
	}

	/**
	 * Check to see if you need to clear pucks
	 */
	public void checkNeedToClearPucks() {
		for (int p = 0; p < getPucks().size(); p++) {
			if (isUnderCenterLine(p)) {
				clearPuckToIdlePosition(p);
			}
		}
	}

	/**
	 * Checks to see if the puck has fallen off the table
	 */
	private void checkOffTable() {
		for (int p = 0; p < pucks.size(); p++) {
			if (!pucks.get(p).isInPlay())
				continue;

			Vector3 cp = new Vector3();
			pucks.get(p).getpModInst().transform.getTranslation(cp);

			if (cp.z > deck.getHalfWidth()) {
				pucks.get(p).setRotation(rot.RIGHT);
				startPuckDropping(p);
			}

			if (cp.z < -deck.getHalfWidth()) {
				pucks.get(p).setRotation(rot.LEFT);
				startPuckDropping(p);
			}

			if (cp.x > deck.getLength()) {
				pucks.get(p).setRotation(rot.BACK);
				startPuckDropping(p);
			}

			if (cp.y < puck_start_up - deck.getHeight()) {
				clearPuckToIdlePosition(p);
			}
		}
	}

	/**
	 * Check if any pucks are in motion
	 * 
	 * @return true if in motion
	 */
	public boolean isPucksInMotion() {
		return pucksInMotion;
	}

	public void setPucksInMotion(boolean pucksInMotion) {
		this.pucksInMotion = pucksInMotion;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public int getCurrentPuck() {
		return currentPuck;
	}

	/**
	 * Set the current puck
	 * 
	 * @param currentPuck
	 */
	public void setCurrentPuck(int currentPuck) {
		this.currentPuck = currentPuck;
	}

	/**
	 * Check if any pucks are moving
	 */
	public void checkInMotion() {
		Iterator<Puck> p = pucks.iterator();

		while (p.hasNext()) {
			Puck tPuck = p.next();
			if (!tPuck.getVelocity().isZero()) {
				pucksInMotion = true;
				return;
			}
		}

		pucksInMotion = false;

		if (currentPuck == this.pucks.size()) {
			gameState = GameState.ROUND_OVER;
			System.out.println("ROUND OVER");
		}
	}

	/**
	 * Print the information of all the pucks
	 * 
	 * @param addInfo
	 *            Additional message for log identification
	 */
	public void printPuckInfo(String addInfo) {
		System.out.println(addInfo);
		for (int puck = 0; puck < pucks.size(); puck++) {
			pucks.get(puck).printPuckInfo();
		}
	}

	/**
	 * Update the positions of the pucks
	 * 
	 * @param delta
	 *            Time delta from last update
	 */
	public void updatePuckPos(float delta) {
		for (int puck = 0; puck < pucks.size(); puck++) {
			pucks.get(puck).update(delta);
		}
	}

	/**
	 * Update the next puck to the fling velocity
	 * 
	 * @param velocity
	 *            Velocity
	 */
	public void shootPuck(Vector3 velocity) {

		if (gameState == GameState.ROUND_OVER) {
			return;
		}

		this.pucks.get(currentPuck).setVelocity(velocity);

		currentPuck++;

		pucksInMotion = true;
		puckIsLoaded = false;
	}

	/**
	 * Place the puck to a new, but in play position, ready for launch
	 * 
	 * @param position
	 *            Final position
	 */
	public void placeInPlayPuck(Vector3 position) {
		Vector3 scaledPosition = position.scl(deck.getWidth()
				/ Gdx.graphics.getWidth());
		scaledPosition.add(new Vector3(this.puck_start_forward,
				this.puck_start_up, -deck.getHalfWidth()));
		pucks.get(currentPuck).getpModInst().transform
				.setToTranslation(scaledPosition);
	}

	/**
	 * Load the next puck in game sequence
	 */
	public void loadNextPuck() {
		puckIsLoaded = true;
		if (gameState != GameState.ROUND_OVER) {
			loadPuckToStartPosition(currentPuck);
			pucks.get(currentPuck).setInPlay(true);
		}
	}

	/**
	 * Put the puck to the starting position for game play
	 * 
	 * @param puckId
	 *            Puck identifier
	 */
	public void loadPuckToStartPosition(int puckId) {
		placeInPlayPuck(new Vector3(0, 0, Gdx.graphics.getWidth() / 2f));
	}

	/**
	 * Puts the puck in the idle and out of play position
	 * 
	 * @param puckId
	 *            Puck ID
	 */
	public void clearPuckToIdlePosition(int puckId) {
		float offset = Puck.getRadius() * puckId * 2.5f;
		offset -= 100;
		Vector3 position = new Vector3(offset, this.puck_start_up, puckId * 1.1f);
		this.pucks.get(puckId).getpModInst().transform.setTranslation(position);
		this.pucks.get(puckId).setInPlay(false);
		this.pucks.get(puckId).setVelocity(new Vector3(0, 0, 0));
	}

	/**
	 * Count the pucks from
	 */
	public void scoreThePucks() {
		List<Puck> sPucks = new ArrayList<Puck>();
		Iterator<Puck> p = getPucks().iterator();

		while (p.hasNext()) {
			Puck tPuck = p.next();
			if (tPuck.isInPlay()) {
				sPucks.add(tPuck);
			}
		}

		if (sPucks.size() < 1) {
			// there are no pucks in play, all left the table
			return;
		}

		// make an array of [owner, depth] pairs
		double[][] plist = new double[sPucks.size()][];

		p = sPucks.iterator();
		int pi = 0;
		while (p.hasNext()) {
			Puck tPuck = p.next();
			int owner = tPuck.getOwner().ordinal();
			Vector3 position = new Vector3();
			tPuck.getpModInst().transform.getTranslation(position);
			double depth = position.x;
			plist[pi] = new double[] { (double) owner, depth };
			pi++;
		}

		java.util.Arrays.sort(plist, new java.util.Comparator<double[]>() {
			public int compare(double[] a, double[] b) {
				return Double.compare(a[1], b[1]);
			}
		});

		ScoreKeeper.player ePlayer = player.RED_PLAYER;
		int score = 0;

		for (int px = plist.length - 1; px >= 0; px--) {
			if (px == plist.length - 1) {
				// if it's the first entry
				ePlayer = player.values()[(int) plist[px][0]];
				score += getIndividualScore(plist[px][1]);
			} else {

				if (plist[px][0] != plist[px + 1][0]) {
					break;
				}

				score += getIndividualScore(plist[px][1]);
			}
		}

		this.score.addScore(ePlayer, score);
	}

	public int getIndividualScore(double distance) {

		if (distance > this.getDeck().getFourPointLine()) {
			return 4;
		}

		if (distance > this.getDeck().getThreePointLine()) {
			return 3;
		}

		if (distance > this.getDeck().getTwoPointLine()) {
			return 2;
		}

		if (distance > this.getDeck().getOnePointLine()) {
			return 1;
		}

		return 0;
	}

	/**
	 * Starts the puck dropping, puck falling off the table
	 * 
	 * @param puckId
	 *            Puck ID
	 */
	public void startPuckDropping(int puckId) {
		Vector3 dropVel = new Vector3(0, -1, 0);
		this.pucks.get(puckId).getVelocity().add(dropVel);
	}

	/**
	 * Check if this puck collides with any other
	 * 
	 * @param targ
	 *            target puck
	 */
	public void checkAllPucksCollide(int index, List<Puck> tPucks) {
		if (index + 1 >= tPucks.size())
			return;

		for (int p = index + 1; p < tPucks.size(); p++) {

			if (!pucksCollide(tPucks.get(index), tPucks.get(p))) {
				// pucks missed, continue
				continue;
			}

			// pucks collide
			tPucks.get(index).printPuckInfo();
			tPucks.get(p).printPuckInfo();

			collide(tPucks.get(index), tPucks.get(p));

			tPucks.get(index).printPuckInfo();
			tPucks.get(p).printPuckInfo();
		}

		checkAllPucksCollide(index + 1, tPucks);
	}

	/**
	 * Perform collision of pucks
	 * 
	 * @param a
	 *            Puck A
	 * @param b
	 *            Puck B
	 */
	public void collide(Puck a, Puck b) {
		// Get vector of collision
		Vector3 tVect1 = new Vector3();
		Vector3 tVect2 = new Vector3();
		a.getpModInst().transform.getTranslation(tVect1);
		b.getpModInst().transform.getTranslation(tVect2);

		// a to b
		Vector3 bPath = new Vector3(tVect2.sub(tVect1));

		// Unit vector
		bPath = bPath.nor();
		Vector3 aPath = new Vector3(bPath.scl(-1f));

		// Decompose to components of unit vector
		float d = a.getVelocity().dot(bPath);
		float e = b.getVelocity().dot(aPath);

		Vector3 vB1 = new Vector3(bPath.scl(d));
		Vector3 vA1 = new Vector3(aPath.scl(e));

		// Set puck a velocity
		this.pucks.get(a.getIdent()).setVelocity(a.getVelocity().add(vA1));
		this.pucks.get(a.getIdent()).setVelocity(a.getVelocity().sub(vB1));

		// Set puck b velocity
		this.pucks.get(b.getIdent()).setVelocity(b.getVelocity().add(vB1));
		this.pucks.get(b.getIdent()).setVelocity(b.getVelocity().sub(vA1));
	}

	/**
	 * Checking for puck collisions
	 * 
	 * @param a
	 *            Puck a
	 * @param b
	 *            Puck b
	 * @return boolean true if collide
	 */
	public boolean pucksCollide(Puck a, Puck b) {
		double dist = checkModelDist(a, b);

		if (dist < Puck.getRadius() * 2) {
			// boop boop collision
			System.out.println(String.format("Collision between %d and %d",
					a.getIdent(), b.getIdent()));

			return true;
		}

		return false;
	}

	/**
	 * Check the distance between the two pucks
	 * 
	 * @param a
	 *            Puck a
	 * @param b
	 *            Puck b
	 * @return Distance
	 */
	public float checkModelDist(Puck a, Puck b) {
		Vector3 aPos = new Vector3();
		a.getpModInst().transform.getTranslation(aPos);
		Vector3 bPos = new Vector3();
		b.getpModInst().transform.getTranslation(bPos);
		return aPos.dst(bPos);
	}

	public void dispose() {
		deckModel.dispose();
	}

	// Auto generated getters and setters
	public float getPuck_start_forward() {
		return puck_start_forward;
	}

	public List<Puck> getPucks() {
		return pucks;
	}

	public void setPucks(List<Puck> pucks) {
		this.pucks = pucks;
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}
	
	public boolean isDoneLoading() {
		return doneLoading;
	}

	public void setDoneLoading(boolean doneLoading) {
		this.doneLoading = doneLoading;
	}

}
