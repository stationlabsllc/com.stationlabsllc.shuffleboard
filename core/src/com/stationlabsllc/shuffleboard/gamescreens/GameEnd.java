/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.math.Matrix4;

public class GameEnd extends OverScreen {

	private boolean done = false;
	private final SpriteBatch sprites;
	private final Texture background;
	private final Matrix4 viewMatrix = new Matrix4();
	private final Matrix4 transformMatrix = new Matrix4();

	public GameEnd() {
		System.out.println("Starting Game End Screen");
		this.sprites = new SpriteBatch();
		this.background = new Texture(
				Gdx.files.internal("imgs/woodbackground.jpg"));
		background.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}

	@Override
	public void render(float delta) {
		if (Gdx.input.isTouched()) {
			this.done = true;
		}

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		viewMatrix.setToOrtho2D(0, 0, this.sWidth, this.sHeight);
		sprites.setProjectionMatrix(viewMatrix);
		sprites.setTransformMatrix(transformMatrix);
		sprites.begin();
		sprites.disableBlending();
		sprites.setColor(Color.WHITE);
		sprites.draw(background, 0, 0, this.sWidth, this.sHeight, 0, 0,
				512, 512, false, false);
		sprites.enableBlending();
		String text = "Game Over.\nTouch to continue!";
		sprites.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
		font.drawMultiLine(sprites, text, 0.05f * this.sWidth,
				this.sHeight / 2, this.sWidth, HAlignment.CENTER);
		sprites.end();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		this.font.dispose();
		this.background.dispose();
		this.sprites.dispose();
	}

	@Override
	public boolean isDone() {
		return done;
	}
}
