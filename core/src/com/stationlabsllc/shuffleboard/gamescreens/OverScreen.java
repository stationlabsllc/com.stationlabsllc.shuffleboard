/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public abstract class OverScreen implements Screen {

	protected final int sWidth;
	protected final int sHeight;
	protected final float scaleWidth;
	protected final float scaleHeight;
	protected final float dip;
	protected final float sFontSize;
	protected final BitmapFont font;

	public OverScreen() {
		this.sHeight = Gdx.graphics.getHeight();
		this.sWidth = Gdx.graphics.getWidth();
		this.scaleWidth = this.sWidth / 768f;
		this.scaleHeight = this.sHeight / 1280f;

		this.dip = Gdx.graphics.getDensity();
		this.sFontSize = this.scaleWidth * dip;
		// BitmapFont font = generator.generateFont(fontSize );
		this.font = new BitmapFont(Gdx.files.internal("fonts/belbitfont.fnt"),
				Gdx.files.internal("fonts/belbitfont_0.png"), false);
		this.font.setScale(scaleWidth * dip, scaleHeight * dip);
	}

	public abstract boolean isDone();
}
