/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard.gamescreens;

import com.stationlabsllc.shuffleboard.GameRenderer;
import com.stationlabsllc.shuffleboard.GameWorld;
import com.stationlabsllc.shuffleboard.GameWorld.GameState;
import com.stationlabsllc.shuffleboard.Inputer;
import com.stationlabsllc.shuffleboard.ScoreKeeper;
import com.stationlabsllc.shuffleboard.gamescreens.OverScreen;

public class MainPlay extends OverScreen {

	private boolean done = false;

	/**
	 * Programmatic representation of the game
	 */
	private GameWorld world;

	private Inputer inputer;

	/**
	 * Graphical representation of the game displayed to player
	 */
	private GameRenderer renderer;
	private ScoreKeeper score;

	public MainPlay(ScoreKeeper score) {
		System.out.println("Starting game main play screen");
		this.score = score;
		this.world = new GameWorld(this.sWidth, this.sHeight, this.score);
		this.inputer = new Inputer(this.world);
		this.renderer = new GameRenderer(this.world);
	}

	@Override
	public void render(float delta) {

		// Updating the world 2 has collision detection advantages but slows
		// rendering rate, may need to revisit later
		final int splitNo = 2;
		float splitDelta = delta / splitNo;

		for (int cSplit = 0; cSplit < splitNo; cSplit++) {
			this.world.update(splitDelta);
		}

		this.renderer.render();

		if (world.getGameState() == GameState.ROUND_OVER){
			world.scoreThePucks();
			done = true;
		}
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		inputer.dispose();
		world.dispose();
		renderer.dipose();
	}

	@Override
	public boolean isDone() {
		return done;
	}

}
