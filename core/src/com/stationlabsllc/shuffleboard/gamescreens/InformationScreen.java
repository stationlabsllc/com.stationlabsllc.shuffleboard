/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.math.Matrix4;

public class InformationScreen extends OverScreen {

	final private String rule1 = "1 Tap to move the puck";
	final private String rule2 = "2 Slide and lift to shoot the puck";
	final private String rule3 = "3 Pucks before the foul line will be cleared";
	final private String rule4 = "4 Pucks across the respective point line will be scored accordingly";
	final private String rule5 = "5 A puck hanging off the far end of the table is scored as 4";
	final private String rule6 = "5 Only a players furthest pucks, beyoned apponents pucks, will be scored";
	final private String rule7 = "6 The player who scored in the previous round, shoots first";

	final private String[] rules = { rule1, rule2, rule3, rule4, rule5, rule6,
			rule7 };

	final private String inst = "Tap to clear";

	private boolean done = false;

	private final SpriteBatch sprites;
	private final Texture background;
	private final Matrix4 viewMatrix = new Matrix4();
	private final Matrix4 transformMatrix = new Matrix4();

	public InformationScreen() {
		System.out.println("Information screen");
		this.sprites = new SpriteBatch();
		this.background = new Texture(
				Gdx.files.internal("imgs/woodbackground.jpg"));
		background.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}

	@Override
	public void render(float delta) {
		if (Gdx.input.isTouched()) {
			this.done = true;
		}

		justDraw(delta);
	}

	private void justDraw(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		viewMatrix.setToOrtho2D(0, 0, this.sWidth, this.sHeight);
		sprites.setProjectionMatrix(viewMatrix);
		sprites.setTransformMatrix(transformMatrix);
		sprites.begin();
		sprites.disableBlending();
		sprites.setColor(Color.WHITE);
		sprites.draw(background, 0, 0, this.sWidth, this.sHeight, 0, 0, 512,
				512, false, false);
		sprites.enableBlending();
		
		// TextBounds bounds = font.getMultiLineBounds(text);
		sprites.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA);
		for (int rule = 0; rule < rules.length; rule++) {
			font.drawMultiLine(sprites, rules[rule], 0.05f * this.sWidth, this.sHeight
					/ 2 + rule * .25f, this.sWidth, HAlignment.CENTER);
		}

		font.drawMultiLine(sprites, this.inst, 0.05f * this.sWidth, this.sHeight
				/ 2, this.sWidth, HAlignment.CENTER);
		
		sprites.end();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		/*
		 * Tap left or right to move the puck. Slide and lift your finger to
		 * shoot the puck
		 */

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDone() {
		return this.done;
	}

}
