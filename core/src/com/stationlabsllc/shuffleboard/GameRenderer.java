/*************************************************************************
 * 
 * Station Labs, LLC.
 * __________________
 * 
 *  [2014] Station Labs, LLC.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Station Labs, LLC. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Station Labs, LLC.
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Station Labs, LLC.
 */

package com.stationlabsllc.shuffleboard;

import java.util.Iterator;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Vector3;
import com.stationlabsllc.shuffleboard.GameWorld.GameState;
import com.stationlabsllc.shuffleboard.gamepieces.Puck;

public class GameRenderer {

	private GameWorld world;
	private PerspectiveCamera cam;
	private ModelBatch modelBatch;
	private Environment enviro;
	private final float backSet = -50f;
	private final float heightSet = 65f;
	private final float foreLook = 60f;

	public GameRenderer(GameWorld world) {
		this.world = world;

		this.enviro = new Environment();
		this.enviro.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f,
				0.4f, 0.4f, 1f));
		this.enviro.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f,
				-0.2f, -0.8f));

		this.modelBatch = new ModelBatch();

		this.cam = new PerspectiveCamera(67f, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		this.cam.position.set(backSet, heightSet, 0f);
		this.cam.lookAt(foreLook, 0f, 0f);
		this.cam.near = 5f;
		this.cam.far = 300f;
		this.cam.update();	
	}

	public void render() {
		
		if (world.getGameState() == GameState.ROUND_OVER){
			return;
		}
		
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		reUpCamera();
		renderDeck();
		renderPucks();
	}

	private void reUpCamera() {
		Vector3 position = new Vector3();
		int followPuck = world.getCurrentPuck();

		if (world.isPucksInMotion()) {
			followPuck -= 1;
		}

		world.getPucks().get(followPuck).getpModInst().transform
				.getTranslation(position);

		float tempBackSet = backSet + position.x
				- world.getPuck_start_forward();
		
		if (tempBackSet < this.backSet){
			tempBackSet = this.backSet;
		}

		cam.position.set(tempBackSet, heightSet, 0f);
		cam.update();
	}

	private void renderDeck() {
		modelBatch.begin(cam);
		modelBatch.render(this.world.getDeck().getpDeck(), enviro);
		modelBatch.end();
	}

	private void renderPucks() {
		modelBatch.begin(cam);
		Iterator<Puck> puckIt = this.world.getPucks().iterator();
		while (puckIt.hasNext()) {
			Puck tPuck = puckIt.next();
			modelBatch.render(tPuck.getpModInst(), enviro);
		}

		modelBatch.end();
	}

	public void dipose() {
		modelBatch.dispose();
	}
}
