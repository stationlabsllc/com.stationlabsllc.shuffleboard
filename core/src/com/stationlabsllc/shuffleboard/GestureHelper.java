package com.stationlabsllc.shuffleboard;

import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GestureHelper implements GestureListener {

	private GameWorld world;

	public GestureHelper(GameWorld world) {
		this.world = world;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {

		if (!isOkForInput()){
			return true;
		}
		
		if (button == 0) {
			System.out.println(String.format("Position (%2.3f,  %2.3f)", x, y));
			float sx = world.getDeck().getScaledPosition(x);
			System.out.println(String.format("Scaled x %2.3f", sx));
			world.placeInPlayPuck(new Vector3(0, 0, x));
		}

		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {


		if (!isOkForInput() || velocityY > 0){
			return true;
		}
		
		if (button == 0) {
			System.out.println(String.format("Vector (%2.3f, %2.3f)",
					velocityX, velocityY));
			final float scal = 0.05f;
			velocityX *= scal;
			velocityY *= -scal;

			world.shootPuck(new Vector3(velocityY, 0, velocityX));

			return true;
		}

		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean isOkForInput(){
		boolean isOk = !world.isPucksInMotion();
		isOk = isOk && world.isDoneLoading();
		return isOk;
	}

}
