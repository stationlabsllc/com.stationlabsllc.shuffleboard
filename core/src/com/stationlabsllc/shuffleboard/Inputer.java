package com.stationlabsllc.shuffleboard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;

public class Inputer {

	private GestureDetector gd;
	private GestureHelper listener;
	
	public Inputer(GameWorld world) {

		listener = new GestureHelper(world);
		gd = new GestureDetector(listener);

		Gdx.input.setInputProcessor(gd);
	}

	public void dispose() {
		gd.cancel();
		Gdx.input.setInputProcessor(null);
	}

}