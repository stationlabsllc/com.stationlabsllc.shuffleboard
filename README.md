# com.stationlabllc.shuffleboard README #

Welcome to the Station Labs, LLC. implementation of shuffle board. This project was started because an adequate simulation of shuffleboard could not be found.  Other simulations lacked realistic physics in collisions of pucks and also boundary conditions such as pucks off the table.

## Prerequisites ##

Below is a list of assumptions of software that we assume you have and are familiar with.  If you don't have the software below, they each have excellent readme and installation guides.

* Java SDK (6 or higher preferred)
* Android ADT
* Eclipse (note that a pre-configured version comes with the Android SDK)
* The gradle / eclipse plug in for fist time build
* egit, pre-installed in the Android SDK version
* Command line git
* Blender (only if you plan on editing models)
* Gimp (only if you plan on editing models)

If you plan on using an alternate configuration, you may have unique problems.  We will address them as they come up.

## General Background ##

This implementation of shuffleboard is build on the LibGDX platform.  The LibGDX components are managed via the gradle automated build environment.  In this project, though eclipse will automatically add Android libraries to a project, LibGDX libraries and pure Java libraries are the only ones used.  Build problems can occur when other libraries are implemented.

## Typical set up ##

* Import the project from Eclipse
* File > Import > eGit
* Select from URL
* Enter URL to this repo
* Enter credentials
* Import gradle project
* Navigate to and open the downloaded folder 
* typically; ~\git\com.stationlabsllc.shufflboard\
* Build gradle project
* If there are any issues, see appropriate section in this file
* Copy the folder *sdk\extras\google\google_play_services to your working directory
* Import the copied folder as an existing project
* From the android solution, go to properties > android > add the play services as a reference
* Finish

## Contribution guidelines ##

* Do all of your work on a branch
* Branch the main code before starting *git checkout -b branchname*
* Merge any changes from the master to your branch to verify operation / compatibility *git merge master*
* Push branch up to bitbucket *git push origin branchname*
* Request code review, **do not merge to master**
* Request integration
* Track your work
* Use the issue tracker to log and work on bugs

# FAQ #

## Problems Building ##

### Java not found ###

Common sources of Java not being found, specifically the JDK (SDK) not being found is that the JDK is not installed.  The JDK is not a part of a typically Java installation, you need to specifically have installed it and chances are, unless you're doing other Java development, it has not been installed.  Visit the Oracle / Sun website for the JDK download.

If you are sure you have installed the JDK, the issue may be that multiple Java versions or JDKs have been installed causing problems with your path variables.  Verify that the proper java version is included in your path, typically under JAVA.

### Android SDK not found ###

If you have downloaded an alternate Eclipse version than the version included with the Anrdoid ADT / SDK bundle, you may encounter the issue that the SDK is not found.  Simply go to the properties > Android > SDK and navigate to the location of the SDK folder.

### Problems with ROBO X ###

Upgrade to the latest Java JDK, there are problems with the older versions, this is purely a gradle issue and required for iOS development.  Windows and Linux environments can build all the projects except the iOS version, to build an iOS application, you must be using iOS already.  So as an alternative, if you don't care about the iOS portion (because you are not developing on iOS), you can remove the project("ios") section from the build.gradle file.  Do not commit this modification though as iOS will be supported, the modification is purely a setup option.

## Problems Running ##

Gradle projects are unique, you cannot run the core directly as with most applications.  You must run the target environment, LibGDX does the porting.  For example, most testing happens on the desktop, so open the desktop version, right click it and choose run as a Java application.

### Android Platform Not Found ###

Android and the ADT do not come with a platform by default, you must build one.  You can launch the platform manager internally from eclipse, there should be a button or menu item called AVD manager (Android Virtual Device Manager) which will allow you to build the desired platform.  A better method would be to actually use an Android device, things will be much faster, smoother, and the android emulator isn't a perfect case to model anyway.

## Other Resources ##

For nearly all other issues, the repo admin can be contacted via the bitbucket site or email.  If you are detached from the main repo, IE some other entity working outside of station labs, you will not be supported.